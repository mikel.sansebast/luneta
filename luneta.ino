/*  ----------------------------------------------------------------
 Ahokatu ondoren argi horia piztuko zaigu jakinarazteko zirkuitua martxan dagoela. 
 Arduino joystick-a eskubirantz mugitzen badugu argi horia itzaldu eta berdea piztuko da. Honekin batera motorra martxan jarriko da 
 eskubirantz biratzen.
 Beste aldera eraman eskero joystick-a  argi berdea itzaliko da eta gorria piztuko da eta motorra ezkerrerantz 
 hasiko da biratzen.
 
--------------------------------------------------------------------  
*/
#define IN1  12
#define IN2  11
#define IN3  10
#define IN4  9

int steps_left=4095;         // Pauso gehiago edo gutxiago definitzeko parametroa
boolean Direction = true;    
int Steps = 0;

// Pauso erdiko Matrizea. Beste kontroletarako matrizea aldatu.

int Paso [ 8 ][ 4 ] =        
    {   {1, 0, 0, 0},
        {1, 1, 0, 0},
        {0, 1, 0, 0},
        {0, 1, 1, 0},
        {0, 0, 1, 0},
        {0, 0, 1, 1},
        {0, 0, 0, 1},
        {1, 0, 0, 1}
     };
     
int x = 0;

void setup()
    {
      Serial.begin(115200);
      pinMode(IN1, OUTPUT); 
      pinMode(IN2, OUTPUT); 
      pinMode(IN3, OUTPUT); 
      pinMode(IN4, OUTPUT); 
      pinMode(6, OUTPUT);
      pinMode(7, OUTPUT);
      pinMode(5, OUTPUT);
    }

void loop() {     
     x = analogRead(0);           // Irakurketa 10 bit-etan (0-1023) balio artean
     Serial.print("Joystic:   ");
     Serial.println(x); 
     if(x>=612)
         { Direction=true;
           stepper() ;     // Pauso bat aurrera
           steps_left-- ;  // Pauso bat gutxiago egin da.
           delay (1) ;
           digitalWrite(6, HIGH);                      
           digitalWrite(7, LOW); 
           digitalWrite(5,LOW); 
         }
     else if (x<=412)
         { Direction=false;
           stepper() ;     // Pauso bat aurrera
           steps_left++ ;  // Pauso bat gutxiago egin da.
           delay (1) ;
           digitalWrite(6, LOW);                      
           digitalWrite(7, HIGH);  
           digitalWrite(5,LOW); 
         }
     else { 
           digitalWrite(7, LOW); 
           digitalWrite(6, LOW);                      
           digitalWrite(5, HIGH);  
         } 
       delay(0);
}

void stepper()              //Pauso bat aurrera egiten duen Funtzioa
{
  digitalWrite( IN1, Paso[Steps][ 0] );
  digitalWrite( IN2, Paso[Steps][ 1] );
  digitalWrite( IN3, Paso[Steps][ 2] );
  digitalWrite( IN4, Paso[Steps][ 3] );
  SetDirection();

  
  Serial.print("Pausoak:  ");
  Serial.println(steps_left);
  Serial.print("Pausoa:  ");
  Serial.println(Steps);
  Serial.print("Norantza:  ");
  Serial.println(Direction);
  Serial.println("");
  
}

// Norantzaren arabera bobinen eszitazio norantza definitzen duen Funtzioa
void SetDirection()     
{
    if(Direction)
        Steps++;
    else 
        Steps--; 
     
    Steps = ( Steps + 8 ) % 8 ;
}